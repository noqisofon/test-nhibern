﻿//
//  Copyright 2016  ned rihine
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;
using System.Collections.Generic;
using System.Reflection;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Engine;
using NUnit.Framework;
using Test.Hibern.Domain;

using NHibernateEnvironment = NHibernate.Cfg.Environment;


namespace Test.Hibern {


    /// <summary>
    /// Test.
    /// </summary>
    [TestFixture]
    public class Test {

        #region Protected Properties

        /// <summary>
        /// Gets the name of the mapping assembly.
        /// </summary>
        /// <value>The name of the mapping assembly.</value>
        protected virtual string MappingAssemblyName {
            get { return "Test.Hibern"; }
        }


        /// <summary>
        /// Sets the cache concurrency strategy.
        /// </summary>
        /// <value>The cache concurrency strategy.</value>
        protected virtual string CacheConcurrencyStrategy {
            get { return "nonstrict-read-write"; }
        }


        /// <summary>
        /// Gets the mapping file names.
        /// </summary>
        /// <value>The mapping file names.</value>
        protected virtual IEnumerable<string> MappingFileNames {
            get { return new [] { "Person.hdm.xml" }; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Setup this instance.
        /// </summary>
        [OneTimeSetUp]
        public void Setup() {
            Configure();
        }


        /// <summary>
        /// Fills the db.
        /// </summary>
        [Test]
        public void fill_db() {
            using ( var session = factory_.OpenSession() ) {
                using ( var transaction = session.BeginTransaction() ) {
                    session.Save( new Person { Id = 1, Name = "hoge", Age = 16 } );
                    session.Save( new Person { Id = 2, Name = "piyo", Age = 17 } );

                    transaction.Commit();
                }
            }
        }

        #endregion


        #region Protected Methods

        /// <summary>
        /// Builds the configuration.
        /// </summary>
        /// <returns>The configuration.</returns>
        protected virtual Configuration BuildConfiguration() {
            var config = new Configuration();
            {
                config.Configure( "nhibernate.config" );
                config.AddAssembly( this.MappingAssemblyName );
            }
            return config;
        }


        /// <summary>
        /// Adds the mappings.
        /// </summary>
        /// <param name="config">Config.</param>
        protected virtual void AddMappings(Configuration config) {
            foreach ( var fileName in MappingFileNames ) {
                config.AddFile( fileName );
            }
        }


        /// <summary>
        /// Applies the cache settings.
        /// </summary>
        /// <param name="config">Config.</param>
        protected virtual void ApplyCacheSettings(Configuration config) {
            if ( string.IsNullOrEmpty( this.CacheConcurrencyStrategy ) ) {

                return;
            }
        }


        /// <summary>
        /// Configure the specified config.
        /// </summary>
        /// <param name="config">Config.</param>
        protected virtual void Configure(Configuration config) {
            config.SetProperty( NHibernateEnvironment.FormatSql, "true" );
            config.SetProperty( NHibernateEnvironment.GenerateStatistics, "true" );
            config.SetProperty( NHibernateEnvironment.BatchSize, "10" );
        }

        /// <summary>
        /// Builds the session factory.
        /// </summary>
        /// <param name="config">Config.</param>
        protected virtual void BuildSessionFactory(Configuration config) {
            factory_ = config.BuildSessionFactory() as ISessionFactoryImplementor;
        }

        #endregion


        #region Private Methods

        /// <summary>
        /// Configure this instance.
        /// </summary>
        private void Configure() {
            var config = BuildConfiguration();
            {
                AddMappings( config );

                Configure( config );

                ApplyCacheSettings( config );
            }
        }

        #endregion


        #region Private Fields

        /// <summary>
        /// The factory.
        /// </summary>
        private ISessionFactoryImplementor factory_;
        ///

        #endregion
    }
}

